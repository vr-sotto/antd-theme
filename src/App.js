import React from "react";
import logo from "./logo.svg";

import { Layout, Button, Menu, Icon, Row, Col } from "antd";

const { Header, Footer, Sider, Content } = Layout;
const SubMenu = Menu.SubMenu;

class App extends React.Component {
  // submenu keys of first level
  rootSubmenuKeys = ["sub1", "sub2", "sub4"];

  state = {
    openKeys: ["sub1"]
  };

  onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(
      key => this.state.openKeys.indexOf(key) === -1
    );
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : []
      });
    }
  };

  render() {
    return (
      <Layout>
        <Sider
          width="235"
          style={{ height: "100vh" }}
          theme="light"
          className="p-lr-15"
        >
          <Menu
            mode="inline"
            // openKeys={this.state.openKeys}
            onOpenChange={this.onOpenChange}
            style={{ marginTop: 64 }}
          >
            <Menu.Item key="1">
              <Icon type="mail" />
              Navigation One
            </Menu.Item>

            <SubMenu
              key="sub2"
              title={
                <span>
                  <Icon type="appstore" />
                  <span>Navigation Two</span>
                </span>
              }
            >
              <Menu.Item key="5">Option 5</Menu.Item>
              <Menu.Item key="6">Option 6</Menu.Item>
              <SubMenu key="sub3" title="Submenu">
                <Menu.Item key="7">Option 7</Menu.Item>
                <Menu.Item key="8">Option 8</Menu.Item>
              </SubMenu>
            </SubMenu>
            <SubMenu
              key="sub4"
              title={
                <span>
                  <Icon type="setting" />
                  <span>Navigation Three</span>
                </span>
              }
            >
              <Menu.Item key="9">Option 9</Menu.Item>
              <Menu.Item key="10">Option 10</Menu.Item>
              <Menu.Item key="11">Option 11</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout className="p-lr-15">
          <Header className="p-0">Header</Header>
          <Content>
            <Row>
              <Col span={24}>
                <div>
                  <Button type="primary" className="btn-override">
                    Test
                  </Button>
                </div>
              </Col>
            </Row>
          </Content>
          <Footer className="p-0">Footer</Footer>
        </Layout>
      </Layout>
    );
  }
}

export default App;
